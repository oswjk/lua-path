-- Mock LuaFileSystem
package.loaded.lfs = {
    currentdir = function ()
        return nil
    end
}

local pl = require("path")

local testcases = {}
local setups = {}
local teardowns = {}

-- This function registers the named testfunction to be executed
local function test(name, func)
    table.insert(testcases, {name=name, func=func})
end

-- Register a setup function for a test
local function setup(name, func)
    setups[name] = func
end

-- Register a teardown function for a test
local function teardown(name, func)
    teardowns[name] = func
end

-- Function for comparing values deeply.
local function deepeq(a, b)
    if a == b then
        return true
    end

    if type(a) ~= "table" or type(b) ~= "table" then
        return false
    end

    for k, va in pairs(a) do
        local vb = b[k]
        if va ~= vb and not deepeq(va, vb) then
            return false
        end
    end

    for k, v in pairs(b) do
        if a[k] == nil then
            return false
        end
    end

    return true
end

-- Recursive tostring().
local function deeptostring(o)
    if type(o) ~= "table" then
        return tostring(o)
    end

    local result = "{"

    for k, v in pairs(o) do
        result = result .. k .. "=" .. deeptostring(v) .. ","
    end

    return result .. "}"
end

local function assert_equal(actual, expected)
    io.write(".")
    if not deepeq(actual, expected) then
        error(string.format("%s != %s", deeptostring(actual), deeptostring(expected)), 2)
    end
end

local function assert_true(value)
    io.write(".")
    if not value then
        error("not true", 2)
    end
end

local function assert_false(value)
    io.write(".")
    if value then
        error("not false", 2)
    end
end

local function assert_error(func)
    local ok = pcall(func)
    io.write(".")
    if ok then
        error("expected an error", 2)
    end
end

test('windows.splitdrive', function ()
    local splitdrive = pl.windows.splitdrive
    assert_equal({splitdrive([[C:\Users\JohnDoe]])}, {"C:", [[\Users\JohnDoe]]})
    assert_equal({splitdrive([[C:Users\JohnDoe]])}, {"C:", [[Users\JohnDoe]]})
    assert_equal({splitdrive([[C:..\Users\JohnDoe]])}, {"C:", [[..\Users\JohnDoe]]})
    assert_equal({splitdrive([[C:.\Users\JohnDoe]])}, {"C:", [[.\Users\JohnDoe]]})
    assert_equal({splitdrive([[\\SERVER\Share\Documents]])}, {[[\\SERVER\Share]], [[\Documents]]})
    assert_equal({splitdrive([[\\SERVER\Share\]])}, {[[\\SERVER\Share]], [[\]]})
    assert_equal({splitdrive([[\\SERVER\Share]])}, {[[\\SERVER\Share]], [[]]})
end)

test('windows.normpath', function ()
    local norm = pl.windows.normpath

    -- Special prefixes should pass right through
    assert_equal(norm([[\\?\C:\Users\.\JohnDoe]]), [[\\?\C:\Users\.\JohnDoe]])
    assert_equal(norm([[\\.\Some\\Device\..\Foobar]]), [[\\.\Some\\Device\..\Foobar]])

    assert_equal(norm([[C:\Users\\JohnDoe]]), [[C:\Users\JohnDoe]])
    assert_equal(norm([[C:\Users\JohnDoe\.\My Pictures]]), [[C:\Users\JohnDoe\My Pictures]])
    assert_equal(norm([[C:\Users\JohnDoe\..\JaneDoe]]), [[C:\Users\JaneDoe]])
    assert_equal(norm([[C:/Users/JohnDoe]]), [[C:\Users\JohnDoe]])
    assert_equal(norm([[..\foo\bar]]), [[..\foo\bar]])
    assert_equal(norm([[.\foo\bar]]), [[foo\bar]])
    assert_equal(norm([[C:\foo\bar\..\..]]), [[C:\]])
    assert_equal(norm([[C:\foo\bar\..\..\..]]), [[C:\]])
    assert_equal(norm([[..\..]]), [[..\..]])
    assert_equal(norm([[C:\..\foo\bar]]), [[C:\foo\bar]])

    assert_equal(norm([[\\SERVER\Shared\Documents]]), [[\\SERVER\Shared\Documents]])
    assert_equal(norm([[\\SERVER\Shared\Documents\..]]), [[\\SERVER\Shared\]])
    assert_equal(norm([[\\SERVER\Shared\Documents\Foo\..]]), [[\\SERVER\Shared\Documents]])
    assert_equal(norm([[\\SERVER\Shared\Documents\Foo\..\..]]), [[\\SERVER\Shared\]])
    assert_equal(norm([[\\SERVER\Shared\Documents\Foo\..\..\..]]), [[\\SERVER\Shared\]])
end)

test('windows.isabs', function ()
    local isabs = pl.windows.isabs

    assert_true(isabs([[C:\Users\JohnDoe]]))
    assert_true(isabs([[\Users\JohnDoe]]))
    assert_true(isabs([[\\SERVER\Share\Users\JohnDoe]]))

    assert_false(isabs([[C:Users\JohnDoe]]))
    assert_false(isabs([[C:..\Users\JohnDoe]]))
end)

test('windows.split', function ()
    local split = pl.windows.split
    assert_equal({split[[C:\Users\JohnDoe]]}, {[[C:\Users]], [[JohnDoe]]})
    assert_equal({split[[C:\Users\JohnDoe\]]}, {[[C:\Users\JohnDoe]], [[]]})
    assert_equal({split[[C:\Users\JohnDoe\\\\\Foobar]]}, {[[C:\Users\JohnDoe]], [[Foobar]]})
    assert_equal({split[[\\SERVER\\Share\Documents\Foobar]]}, {[[\\SERVER\\Share\Documents]], [[Foobar]]})
    assert_equal({split[[\\SERVER\\Share\Documents\\\\\Foobar]]}, {[[\\SERVER\\Share\Documents]], [[Foobar]]})
    assert_equal({split[[C:\Users]]}, {[[C:\]], [[Users]]})
    assert_equal({split[[Foo\Bar\Baz]]}, {[[Foo\Bar]], [[Baz]]})
end)

test('windows.dirname', function ()
    local dirname = pl.windows.dirname
    assert_equal(dirname[[C:\Users\JohnDoe]], [[C:\Users]])
    assert_equal(dirname[[C:\Users\]], [[C:\Users]])
end)

test('windows.basename', function ()
    local basename = pl.windows.basename
    assert_equal(basename[[C:\Users\JohnDoe]], [[JohnDoe]])
    assert_equal(basename[[C:\Users\]], [[]])
end)

test('windows.splitext', function ()
    local splitext = pl.windows.splitext
    assert_equal({splitext".bashrc"}, {".bashrc", ""})
    assert_equal({splitext".app.rc"}, {".app", ".rc"})
    assert_equal({splitext"foobar.txt"}, {"foobar", ".txt"})
    assert_equal({splitext"foobar"}, {"foobar", ""})
    assert_equal({splitext[[C:\Users\JohnDoe\foo.txt]]}, {[[C:\Users\JohnDoe\foo]], ".txt"})
    assert_equal({splitext[[C:\Users\JohnDoe\.txt.dat]]}, {[[C:\Users\JohnDoe\.txt]], ".dat"})
end)

setup('windows.expanduser', function(state)
    state.os_getenv = os.getenv
    state.env = {
        HOME = nil,
        USERPROFILE = nil,
        HOMEPATH = nil,
        HOMEDRIVE = nil,
    }
    os.getenv = function (varname) return state.env[varname] end
end)

teardown('windows.expanduser', function(state)
    os.getenv = state.os_getenv
end)

test('windows.expanduser.no_tilde', function (state)
    local expanduser = pl.windows.expanduser
    assert_equal(expanduser([[C:\Foobar]]), [[C:\Foobar]])
    assert_equal(expanduser([[foobar]]), [[foobar]])
end)

test('windows.expanduser.HOME', function (state)
    local expanduser = pl.windows.expanduser
    state.env.HOME = [[C:\Users\JohnDoe]]
    assert_equal(expanduser([[~/Documents]]), [[C:\Users\JohnDoe/Documents]])
    assert_equal(expanduser([[~JaneDoe]]), [[C:\Users\JaneDoe]])
    assert_equal(expanduser([[~JaneDoe/Documents]]), [[C:\Users\JaneDoe/Documents]])
end)

test('windows.expanduser.USERPROFILE', function (state)
    local expanduser = pl.windows.expanduser
    state.env.USERPROFILE = [[C:\Users\JohnDoe]]
    assert_equal(expanduser([[~/Documents]]), [[C:\Users\JohnDoe/Documents]])
    assert_equal(expanduser([[~JaneDoe]]), [[C:\Users\JaneDoe]])
    assert_equal(expanduser([[~JaneDoe/Documents]]), [[C:\Users\JaneDoe/Documents]])
end)

test('windows.expanduser.HOMEPATH_and_HOMEDRIVE', function (state)
    local expanduser = pl.windows.expanduser
    state.env.HOMEPATH = [[\Users\JohnDoe]]
    state.env.HOMEDRIVE = [[C:]]
    assert_equal(expanduser([[~/Documents]]), [[C:\Users\JohnDoe/Documents]])
    assert_equal(expanduser([[~JaneDoe]]), [[C:\Users\JaneDoe]])
    assert_equal(expanduser([[~JaneDoe/Documents]]), [[C:\Users\JaneDoe/Documents]])
end)

test('windows.expanduser.HOMEPATH_no_HOMEDRIVE', function (state)
    local expanduser = pl.windows.expanduser
    state.env.HOMEPATH = [[\Users\JohnDoe]]
    assert_equal(expanduser([[~/Documents]]), [[\Users\JohnDoe/Documents]])
    assert_equal(expanduser([[~JaneDoe]]), [[\Users\JaneDoe]])
    assert_equal(expanduser([[~JaneDoe/Documents]]), [[\Users\JaneDoe/Documents]])
end)

test('windows.expanduser.no_environment', function (state)
    local expanduser = pl.windows.expanduser
    -- No environment variables available
    assert_equal(expanduser([[~/Documents]]), [[~/Documents]])
end)

setup('windows.abspath', function()
    package.loaded.lfs.currentdir = function ()
        return [[C:\Users]]
    end
end)

test('windows.abspath', function ()
    local abspath = pl.windows.abspath
    assert_equal(abspath([[JohnDoe]]), [[C:\Users\JohnDoe]])
    assert_equal(abspath([[.\JohnDoe]]), [[C:\Users\JohnDoe]])
    assert_equal(abspath([[\JohnDoe]]), [[C:\JohnDoe]])
    assert_equal(abspath([[..]]), [[C:\]])
    assert_equal(abspath([[.]]), [[C:\Users]])
    assert_equal(abspath([[C:\Foobar\Baz]]), [[C:\Foobar\Baz]])
end)

setup('windows.relpath', function()
    package.loaded.lfs.currentdir = function ()
        return [[C:\Users\JohnDoe\Documents\PDFs]]
    end
end)

test('windows.relpath', function ()
    local relpath = pl.windows.relpath
    assert_equal(relpath([[C:\Users\JohnDoe\Documents]]), [[..]])
    assert_equal(relpath([[C:\Users\JohnDoe]]), [[..\..]])
    assert_equal(relpath([[C:\Users]]), [[..\..\..]])
    assert_equal(relpath([[C:\Users\JaneDoe]]), [[..\..\..\JaneDoe]])

    assert_equal(relpath([[C:\Users\JohnDoe\Documents\PDFs]]), [[.]])

    assert_equal(relpath([[C:\Users\JohnDoe\Documents\PDFs\Foobar]]), [[Foobar]])

    assert_equal(relpath([[C:\Windows\System32]]), [[..\..\..\..\Windows\System32]])

    assert_error(function () relpath([[D:\Storage]]) end)
end)

test('windows.join', function ()
    local join = pl.windows.join
    assert_equal(join([[C:\Users]], "JohnDoe", "My Documents"), [[C:\Users\JohnDoe\My Documents]])
    assert_equal(join([[C:\Users]], "JohnDoe", "My Documents", [[\Windows]]), [[C:\Windows]])
    assert_equal(join([[C:\Users\JohnDoe]], [[\Foobar]]), [[C:\Foobar]])
    assert_equal(join([[C:\Users\JohnDoe]], [[D:\Foobar]]), [[D:\Foobar]])
    assert_equal(join([[C:\Users\JohnDoe]], [[D:Foobar]]), [[D:Foobar]])
    assert_equal(join([[Foo]], [[Bar]]), [[Foo\Bar]])
    assert_equal(join([[C:\Users\JohnDoe]], [[c:\foobar]]), [[c:\foobar]])
end)

test('posix.isabs', function()
    assert_false(pl.posix.isabs('foo/bar'))
    assert_false(pl.posix.isabs('./foo/bar'))
    assert_false(pl.posix.isabs('../foo/bar'))
    assert_true(pl.posix.isabs('/'))
    assert_true(pl.posix.isabs('/home/john'))
end)

test('posix.join', function()
    assert_equal(pl.posix.join('/', 'home', 'john'), '/home/john')
    assert_equal(pl.posix.join('home', 'john'), 'home/john')
    assert_equal(pl.posix.join('/', 'home', 'john', '/root', 'foobar'), '/root/foobar')
end)

test('posix.split', function()
    assert_equal({pl.posix.split('/home/john/docs')}, {'/home/john', 'docs'})
    assert_equal({pl.posix.split('/home/john/docs/')}, {'/home/john/docs', ''})
    assert_equal({pl.posix.split('docs')}, {'', 'docs'})
    assert_equal({pl.posix.split('/home/john/docs///////file.txt')}, {'/home/john/docs', 'file.txt'})
    assert_equal({pl.posix.split('///////file.txt')}, {'///////', 'file.txt'})
end)

test('posix.splitext', function()
    assert_equal({pl.posix.splitext".bashrc"}, {".bashrc", ""})
    assert_equal({pl.posix.splitext".app.rc"}, {".app", ".rc"})
    assert_equal({pl.posix.splitext"foobar.txt"}, {"foobar", ".txt"})
    assert_equal({pl.posix.splitext"foobar"}, {"foobar", ""})
    assert_equal({pl.posix.splitext[[/home/john/foo.txt]]}, {[[/home/john/foo]], ".txt"})
    assert_equal({pl.posix.splitext[[/home/john/.txt.dat]]}, {[[/home/john/.txt]], ".dat"})
end)

test('posix.splitdrive', function()
    assert_equal({pl.posix.splitdrive("/foo")}, {"", "/foo"})
    assert_equal({pl.posix.splitdrive("foo")}, {"", "foo"})
end)

test('posix.basename', function()
    assert_equal(pl.posix.basename("/home/john"), "john")
    assert_equal(pl.posix.basename("/home/"), "")
    assert_equal(pl.posix.basename("/home"), "home")
    assert_equal(pl.posix.basename("/"), "")
    assert_equal(pl.posix.basename("/home/john//"), "")
end)

test('posix.dirname', function()
    assert_equal(pl.posix.dirname("/home/john"), "/home")
    assert_equal(pl.posix.dirname("/home/john/"), "/home/john")
    assert_equal(pl.posix.dirname("/home/john///"), "/home/john")
    assert_equal(pl.posix.dirname("///home"), "///")
    assert_equal(pl.posix.dirname("///home/"), "///home")
    assert_equal(pl.posix.dirname("/home"), "/")
    assert_equal(pl.posix.dirname("/"), "/")
    assert_equal(pl.posix.dirname("//"), "//")
end)

setup('posix.expanduser', function(state)
    state.os_getenv = os.getenv
    state.env = {
        HOME = nil,
    }
    os.getenv = function (varname) return state.env[varname] end
end)

teardown('posix.expanduser', function(state)
    os.getenv = state.os_getenv
end)

test('posix.expanduser.no_tilde', function (state)
    assert_equal(pl.posix.expanduser([[/usr/bin]]), "/usr/bin")
    assert_equal(pl.posix.expanduser([[foobar]]), [[foobar]])
end)

test('posix.expanduser.HOME', function (state)
    state.env.HOME = [[/home/john]]
    assert_equal(pl.posix.expanduser([[~/docs]]), "/home/john/docs")
    -- assert_equal(pl.posix.expanduser([[~jane]]), "/home/jane")
    -- assert_equal(pl.posix.expanduser([[~jane/docs]]), "/home/jane/docs")
end)

test('posix.expanduser.no_environment', function (state)
    -- No environment variables available
    assert_equal(pl.posix.expanduser([[~/docs]]), [[~/docs]])
end)

test('posix.normpath', function()
    assert_equal(pl.posix.normpath('/home/john/file.txt'), '/home/john/file.txt')
    assert_equal(pl.posix.normpath('/home/john//file.txt'), '/home/john/file.txt')
    assert_equal(pl.posix.normpath('/home//john///file.txt'), '/home/john/file.txt')
    assert_equal(pl.posix.normpath('home/john/file.txt'), 'home/john/file.txt')
    assert_equal(pl.posix.normpath(''), '.')
    assert_equal(pl.posix.normpath('///'), '/')
    assert_equal(pl.posix.normpath('///foo/bar'), '/foo/bar')
    assert_equal(pl.posix.normpath('//'), '//')
    assert_equal(pl.posix.normpath('//foo/bar'), '//foo/bar')
    assert_equal(pl.posix.normpath('/foo/bar/../baz'), '/foo/baz')
    assert_equal(pl.posix.normpath('/foo/bar/.././baz'), '/foo/baz')
    assert_equal(pl.posix.normpath('/foo/bar/.././baz/.'), '/foo/baz')
    assert_equal(pl.posix.normpath('/foo/bar/.././baz/..'), '/foo')
    assert_equal(pl.posix.normpath('/foo/bar/.././baz/../'), '/foo')
    assert_equal(pl.posix.normpath('/foo/bar/.././baz/../..'), '/')
    assert_equal(pl.posix.normpath('/foo/bar/.././baz/../../..'), '/')
    assert_equal(pl.posix.normpath('../foo/bar'), '../foo/bar')
    assert_equal(pl.posix.normpath('../foo/bar/..'), '../foo')
    assert_equal(pl.posix.normpath('./foo/bar'), 'foo/bar')
    assert_equal(pl.posix.normpath('./foo/bar/..'), 'foo')
    assert_equal(pl.posix.normpath('./foo/bar/../..'), '.')
end)

local success, failure = 0, 0

for _, tc in ipairs(testcases) do
    io.write(tc.name .. "\t")

    local setupfuncs, teardownfuncs = {}, {}
    local base
    for w in tc.name:gmatch("[^%.]+") do
        if not base then
            base = w
        else
            base = base .. "." .. w
        end
        if setups[base] then
            table.insert(setupfuncs, setups[base])
        end
        if teardowns[base] then
            table.insert(teardownfuncs, teardowns[base])
        end
    end

    local state = {}

    for _, func in ipairs(setupfuncs) do
        func(state)
    end

    local result, errmsg = pcall(tc.func, state)

    for _, func in ipairs(teardownfuncs) do
        func(state)
    end

    if not result then
        io.write(" FAIL\n")
        io.write(errmsg)
        io.write("\n")
        failure = failure + 1
    else
        io.write(" SUCCESS\n")
        success = success + 1
    end
end

print(string.format("\n%d/%d tests passed", success, success+failure))
