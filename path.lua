local pathlib = {}
local windows = {}
local posix = {}

pathlib.windows = windows
pathlib.posix = posix

local function generic_splitext(path, sep)
    local seppat = '%' .. sep .. '[^%' .. sep .. ']*$'
    local sepindex = path:find(seppat) or 0

    local dotpat = '%.[^%.]+$'
    local dotindex = path:find(dotpat) or 0

    if dotindex > sepindex then
        local fnindex = sepindex + 1
        while fnindex < dotindex do
            if path:sub(fnindex, fnindex) ~= "." then
                return path:sub(1, dotindex-1), path:sub(dotindex)
            end
            fnindex = fnindex + 1
        end
    end

    return path, ""
end

-- Returns a table with either the drive or sharepoint and the rest of the path
-- C:\Users\JohnDoe -> C: and \Users\JohnDoe
-- \\SERVER\\Share\Documents -> \\SERVER\Share and \Documents
function windows.splitdrive(path)
    if #path <= 1 then
        return "", path
    end

    norm = path:gsub("/", "\\")

    -- Check if the path is a normal path with a drive
    if norm:sub(2, 2) == ":" then
        return path:sub(1, 2), path:sub(3)
    end

    -- Check if the path does not resemble unc path
    if norm:sub(1, 2) ~= "\\\\" or norm:sub(3, 3) == "\\" then
        return "", path
    end

    local idx1 = norm:find("\\", 3)
    if not idx1 then
        return "", path
    end

    local idx2 = norm:find("\\", idx1+1)
    if idx2 == idx1 + 1 then
        return "", path
    end

    if not idx2 then
        idx2 = #path+1
    end

    return path:sub(1, idx2-1), path:sub(idx2)
end

function windows.normpath(path)
    if path:sub(1, 4) == [[\\?\]] or path:sub(1, 4) == [[\\.\]] then
        -- Return special paths unmodified
        return path
    end

    path = path:gsub("/", "\\")

    local prefix, path = windows.splitdrive(path)

    -- Collapse initial separators
    if path:sub(1, 1) == "\\" then
        path = path:gsub("^%\\+", "")
        prefix = prefix .. "\\"
    end

    local parts = {}

    -- Iterate over all parts of path separated by separator
    for part in path:gmatch("[^\\]+") do
        if part == "." then
            -- skip
        elseif part == ".." then
            if #parts > 0 and parts[#parts] ~= ".." then
                table.remove(parts)
            elseif #parts == 0 and prefix:sub(#prefix) == "\\" then
                -- Skip, .. does not mean anything when the first thing
                -- after drive spec and initial backslash.
            else
                table.insert(parts, part)
            end
        else
            table.insert(parts, part)
        end
    end

    if #prefix == 0 and #parts == 0 then
        table.insert(parts, ".")
    end

    return prefix .. table.concat(parts, "\\")
end

function windows.isabs(path)
    local prefix, path = windows.splitdrive(path)
    return path:sub(1, 1) == "\\"
end

function windows.split(path)
    local prefix, path = windows.splitdrive(path)

    local i = #path+1
    while i > 1 and path:sub(i-1, i-1) ~= "\\" do
        i = i - 1
    end

    local head = path:sub(1, i-1)
    local tail = path:sub(i)

    -- Remove all trailing slashes, unless the head consists of only slashes
    head = head:gsub([[([^%\])%\+$]], "%1")

    return prefix .. head, tail
end

function windows.splitext(path)
    return generic_splitext(path, "\\")
end

function windows.dirname(path)
    local d, b = windows.split(path)
    return d
end

function windows.basename(path)
    local d, b = windows.split(path)
    return b
end

function windows.expanduser(path)
    if path:sub(1, 1) ~= "~" then
        return path
    end

    local sepindex = path:find("[/\\]", 2)

    if not sepindex then
        sepindex = #path+1
    end

    local userhome = os.getenv("HOME")

    if not userhome then
        userhome = os.getenv("USERPROFILE")
    end

    if not userhome then
        local homepath = os.getenv("HOMEPATH")
        local homedrive = os.getenv("HOMEDRIVE") or ""

        if not homepath then
            return path
        end

        userhome = windows.join(homedrive, homepath)
    end

    if sepindex ~= 2 then
        userhome = windows.join(windows.dirname(userhome),
            path:sub(2, sepindex-1))
    end

    return userhome .. path:sub(sepindex)
end

local _, lfs = pcall(load([[return require("lfs")]]))

local function getcwd()
    if not lfs then
        error("lfs is required")
    end

    return lfs.currentdir()
end

function windows.abspath(path)
    local prefix, rest = windows.splitdrive(path)
    if not windows.isabs(path) or (#prefix == 0 and rest:sub(1, 1) == "\\") then
        path = windows.join(getcwd(), path)
    end
    return windows.normpath(path)
end

function windows.relpath(path, start)
    start = start or "."

    local start_abs = windows.abspath(windows.normpath(start))
    local start_drive, start_rest = windows.splitdrive(start_abs)

    local path_abs = windows.abspath(windows.normpath(path))
    local path_drive, path_rest = windows.splitdrive(path_abs)

    if start_drive:lower() ~= path_drive:lower() then
        error(string.format("different drives: %s and %s", start_drive,
            path_drive))
    end

    local startlst, pathlst = {}, {}

    for p in start_rest:gmatch("[^%\\]+") do
        table.insert(startlst, p)
    end

    for p in path_rest:gmatch("[^%\\]+") do
        table.insert(pathlst, p)
    end

    local common = 0
    for i = 1, math.max(#startlst, #pathlst) do
        local a = startlst[i]
        local b = pathlst[i]

        if (not a) or (not b) or (a:lower() ~= b:lower()) then
            break
        end

        common = common + 1
    end

    local rellst = {}

    for i = 1, #startlst-common do
        table.insert(rellst, "..")
    end

    for i = common+1, #pathlst do
        table.insert(rellst, pathlst[i])
    end

    if #rellst == 0 then
        return "."
    end

    return windows.join(table.unpack(rellst))
end

function windows.join(path, ...)
    local paths = {...}

    prefix, path = windows.splitdrive(path)

    for _, p in ipairs(paths) do
        pprefix, ppath = windows.splitdrive(p)
        if #ppath > 0 and ppath:sub(1, 1) == "\\" then
            -- second path is absolute
            if #pprefix > 0 or #prefix == 0 then
                prefix = pprefix
            end
            path = ppath
        else
            -- second path is relative
            if #pprefix > 0 and pprefix:lower() ~= prefix:lower() then
                -- different drives, use latter
                prefix = pprefix
                path = ppath
            else
                if #pprefix > 0 and pprefix ~= prefix then
                    -- same drive, different case
                    prefix = pprefix
                end
                -- second path is relative to first
                if #path > 0 and path:sub(#path) ~= "\\" then
                    path = path .. "\\"
                end
                path = path .. ppath
            end
        end
    end

    if #path > 0 and path:sub(1, 1) ~= "\\" and #prefix > 0 and prefix:sub(#prefix) ~= ":" then
        return prefix .. "\\" .. path
    end

    return prefix .. path
end

function posix.isabs(path)
    return path:sub(1, 1) == "/"
end

function posix.join(a, ...)
    local path = a
    local p = {...}

    for _,  b in ipairs(p) do
        if b:sub(1, 1) == "/" then
            path = b
        elseif #path == 0 or path:sub(#path) == "/" then
            path = path .. b
        else
            path = path .. "/" .. b
        end
    end

    return path
end

function posix.split(path)
    local i = path:find("%/[^%/]*$")
    i = i or 0
    i = i + 1

    if not i then
        return "", path
    end

    local head = path:sub(1, i-1)
    local tail = path:sub(i)

    if #head > 0 and head:find("[^%/]") then
        head = head:gsub("%/+$", "")
    end

    return head, tail
end

function posix.splitext(path)
    return generic_splitext(path, "/")
end

function posix.splitdrive(path)
    return '', path
end

function posix.basename(path)
    local idx = path:find('[^%/]*$')
    return path:sub(idx)
end

function posix.dirname(path)
    local idx = path:find('[^%/]*$')
    local head = path:sub(1, idx-1)
    if #head > 0 and head:find("[^%/]") then
        head = head:gsub("%/+$", "")
    end
    return head
end

function posix.expanduser(path)
    if path:sub(1, 1) ~= "~" then
        return path
    end

    local idx = path:find("/")

    if not idx then
        idx = #path + 1
    end

    local userhome = os.getenv('HOME')

    if not userhome then
        -- todo: fallback to /etc/passwd?
        return path
    end

    if idx ~= 2 then
        userhome = posix.join(posix.dirname(userhome),
            path:sub(2, idx-1))
    end

    return userhome .. path:sub(idx)
end

function posix.normpath(path)
    if path == "" then
        return "."
    end

    local initial_slashes, initial_slashes_end = path:find("^/+")

    if initial_slashes and initial_slashes_end == initial_slashes + 1 then
        initial_slashes = 2
    elseif initial_slashes then
        initial_slashes = 1
    else
        initial_slashes = 0
    end

    local comps = {}

    for comp in path:gmatch("[^/]*") do
        if comp == "" or comp == "." then
            -- skip
        else
            if comp ~= ".." or (initial_slashes == 0 and #comps == 0) or
                (#comps > 0 and comps[#comps] == "..") then
                comps[#comps+1] = comp
            elseif #comps > 0 then
                table.remove(comps)
            end
        end
    end

    path = table.concat(comps, "/")

    if initial_slashes > 0 then
        path = string.rep("/", initial_slashes) .. path
    end

    if #path == 0 then
        path = "."
    end

    return path
end

function posix.abspath(path)
    if not posix.isabs(path) then
        path = posix.join(getcwd(), path)
    end
    return posix.normpath(path)
end

function posix.relpath(path, start)
    error("todo!")
end

local t

if package.config:sub(1, 1) == "/" then
    t = posix
else
    t = windows
end

for k, v in pairs(t) do
    pathlib[k] = v
end

return pathlib
